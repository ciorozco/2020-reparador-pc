CURSO: Reparador de PC
=============

Docente: Maximiliano Andrada. <br>
Coordinadores: Dr. Cristian Martínez. Lic. Carlos Ismael Orozco

Acerca del curso: El mismo forma parte de una vinculación entre el Departamento de Informática ([DIUNSa](http://di.unsa.edu.ar)) y la Escuela de Arte y Oficio de la Provincia de Salta.

**Condiciones para la obtención de certificado:**
- Certificado de asistencia: un 70% de asistencia mínimo a las clases
- Constancia de aprobación: un 60% de asistencia mínimo a clases y aprobación de un trabajo integrador.

**Temario**

[[_TOC_]]

# Semana 1: Introducción
Clase 1: Presentación
- [Diapositiva](https://gitlab.com/ciorozco/2020-marketing-digitial/-/blob/master/Semana%201/EAO_DIUNSa_MD_clase1.pdf) <br>
- [Video](https://www.youtube.com/watch?v=7HCc_vXsMCQ&feature=youtu.be) <br>

